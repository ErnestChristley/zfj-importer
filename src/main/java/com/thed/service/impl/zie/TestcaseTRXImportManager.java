package com.thed.service.impl.zie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.text.*;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.vfs.FileContent;
import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemManager;
import org.apache.commons.vfs.FileType;
import org.apache.commons.vfs.VFS;

import com.thed.model.JobHistory;
import com.thed.model.Testcase;
import com.thed.zfj.rest.JiraService;

import org.w3c.dom.*;
import javax.xml.parsers.*;


public class TestcaseTRXImportManager extends ImportManagerSupport {
	private final static Log log = LogFactory.getLog(TestcaseWordImportManager.class);
	
	private FileSystemManager fsManager;

	public boolean importAllFiles(TRXImportJob importJob)
			throws Exception {
		try {
			String folderName = importJob.getFolder();
			if (StringUtils.isEmpty(folderName) || !new File(folderName).exists()) {
				throw new FileNotFoundException("Invalid fileName " + folderName);
			}
			fsManager = VFS.getManager();

			String path = null;
			if (folderName.charAt(0) != '\\') // check to check if supplied path is
																				// absoulte path or not (VFS require
																				// absolute path)
				path = "\\" + folderName;
			else
				path = folderName;
			FileObject fileObj = fsManager.resolveFile(path);
			
			System.out.println("Debug: resolved the file path.");
			
			FileObject[] files = null;
			boolean isSuccess = false;
			if (fileObj.getType().equals(FileType.FOLDER)) {
				files = findAllFiles(fileObj, "trx");
			} else {
				files = new FileObject[1];
				files[0] = fileObj;
			}

			boolean currentResult = false;
			boolean lastResult = true;
			boolean allImportFails = false;
			if (files.length > 0) {
				for (FileObject file : files) {
					if (file.getType().equals(FileType.FOLDER)) {
						continue;
					}
					String prev = file.getParent().toString() + File.separator
							+ "success" + File.separator + file.getName().getBaseName();
					FileObject temp = fsManager.resolveFile(prev);
					if (!temp.exists()) {
						System.out.println("Importing the single file " + file);
						currentResult = importSingleFiles(file, importJob);
						lastResult = isImportSuccess(currentResult, lastResult);
						allImportFails = isAllImportsFails(currentResult, allImportFails);
					} else {
						addJobHistory(importJob, file + " already processed");
					}

				}
			} else {
				addJobHistory(importJob, "No files exist.");
			}

			if (files != null && files.length > 1) {
				if (lastResult == true && allImportFails == true) {
					isSuccess = true;
				} else if (lastResult == false && allImportFails == true) {
					isSuccess = true;
				}
				if (!allImportFails) {
					isSuccess = false;
				}
			} else {
				if (currentResult) {
					isSuccess = true;
				}
			}

			return isSuccess;
		} catch (Exception e) {
			addJobHistory(importJob,
					"Exception while performing job " + e.getMessage());
			log.fatal("", e);
			throw e;
		}
	}

	private boolean importSingleFiles(FileObject file, TRXImportJob importJob) {
      try{
		File trxFile = new File(file.getName().getPath());
		
	    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	    Document doc = dBuilder.parse(trxFile);
		
		doc.getDocumentElement().normalize();
		String creator = ((Element)doc.getElementsByTagName("TestRun").item(0)).getAttribute("name");
		creator = creator.split("@")[0].replace('.', ' ');
		
		NodeList nList = doc.getElementsByTagName("UnitTestResult");
		
		for(int i=0; i < nList.getLength(); ++i){
			Node nNode = nList.item(i);
			
			try{

				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					System.out.println("This Unit Test had output.");
					System.out.println("testName : " + eElement.getAttribute("testName"));
					System.out.println("startTime : " + eElement.getAttribute("startTime"));
					String result = eElement.getAttribute("outcome");
					System.out.println("The test " + result);
					if(result.equals("Failed")){
						Element output = (Element)(eElement.getElementsByTagName("Output").item(0));
						Element errorInfo = (Element)(output.getElementsByTagName("ErrorInfo").item(0));
						
						System.out.println("Message : " + errorInfo.getElementsByTagName("Message").item(0).getTextContent());
						System.out.println("StackTrace : " + errorInfo.getElementsByTagName("StackTrace").item(0).getTextContent());
					}
					
					Testcase testcase = new Testcase();
					testcase.setAutomated(true);
					testcase.setName(eElement.getAttribute("testName"));
					testcase.setExternalId(eElement.getAttribute("testName"));
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss.SSSSSSS");
					testcase.setCreationDate(sdf.parse(eElement.getAttribute("startTime").replace('T',' '), new ParsePosition(0)));
					//testcase.setCreator(creator);
					//testcase.setAssignee(creator);  //TODO: need to figure out how to line these two up with Jira
					
					if (StringUtils.isNotBlank(importJob.getComponents())) {
						testcase.components = importJob.getComponents();
					}
					
					if (StringUtils.isNotBlank(importJob.getLabels())) {
						testcase.setTag(importJob.getLabels());
					}
					//TODO:  if there is a Zephyr TEST issue in Jira with the name of this test
					//add a test result to that issue
					//Else, save this test as a new Zephyr TEST issue
					String issueId = JiraService.saveTestcase(testcase);
					JiraService.saveAttachment(issueId, new File(file.getName().getPath()));
					importJob.getHistory().add( new JobHistory(new Date(), "Issue " + issueId + " created!"));
				}
			} catch (Exception e) { e.printStackTrace(); }
		}
		
	  } catch (Exception e){ e.printStackTrace();  }
	  
      return true;
	}


}
