package com.thed.service.impl.zie;

import java.util.LinkedHashSet;
import java.util.Set;

import com.thed.model.HasJobHistory;
import com.thed.model.JobHistory;

public class TRXImportJob implements HasJobHistory {

	private Set<JobHistory> history = new LinkedHashSet<JobHistory>();
	
	private String folder;
	private String components;
	private String labels;
	
	
	public TRXImportJob(Set<JobHistory> history, String folder) {
		super();
		this.history = history;
		this.folder = folder;
		this.components = "";
		this.labels = "";
	}

	@Override
	public Set<JobHistory> getHistory() {
		return history;
	}

	public String getFolder() {
		return folder;
	}

	public String getComponents() {
		return components;
	}

	public String getLabels() {
		return labels;
	}

	
}
