﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Jira.SDK;
using Jira.SDK.Domain;
using JWT;
using System.Net;


//This class would be included in a .Net assembly and work with the MSTest framework to report unit test
//results in real time.
//
//In the [AssemblyInitializ] method, CreateNewTestCycle() would be called with the Jira Project's key
//The Reporting class creates a new cycle, based on the date, and store that and the Project's key
//
//At the beginning of a test, call AddTestToCycle().  Because they are static, the Reporting class already
//knows the project and cycle names.  This method should create the test issue if it does not exist.  The
//test is then added to the cycle.
//
//After each test step, UpdateTestStep() should be called with the test results.  The method should create
//the step if it does not exist.
//
//When properly implemented, I should be able to create a new unit test in VisualStudio, and have it 
//interface with Jira to create what it needs for reporting.


namespace NCTWebPortalUITests.Controllers
{

    public class Reporting
    {
        private static string PROJECT = ""; //must be set in an AssemblyInitialize tagged method
        private static string CYCLE = ""; //by calling CreateNewTestCycle()

        //debugging proxy
        static string debugBaseUrl = "https://private-anon-b4dd3deded-zfjcloud.apiary-proxy.com/connect";
        static string User = <jira username>;
        static string Password = <jira password>;
        //production server
        static string jiraBaseUrl = "<jira url>";

        static string Secret = "<secret that the Zephyr addon provides>";
        static string AccessKey = "<access key that the Zephyr addon provides>";

        public void GetLicenseInfo()
        {
            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/zlicense/addoninfo";
            string query = "";
            var payload = new Dictionary<string, object>() { };

            string responseData = doGet(contextPath, apiPath, query, payload);
        }

        public void UpdateTestCycle(string Version, string CycleName, string Description, string Build, string Environment)
        {
            if (!( CycleExists(CycleName)))
            {
                CreateNewTestCycle(CYCLE);
                return;
            }
            //update the existing cycle here

        }


        /// <summary>
        /// The API call results in a Zephyr error
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        public bool CycleExists(string CycleName)
        {
            //connect to Jira to get project and version IDs using the Jira.SDK library
            Jira.SDK.Jira jira = new Jira.SDK.Jira();
            jira.Connect(jiraBaseUrl, User, Password);
            Project webPortal = jira.GetProject(PROJECT);
            long id = webPortal.ID;
            long version = webPortal.ProjectVersions.First<ProjectVersion>().ID;

            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/cycles/search";
            string query = "expand=&projectId="+id+"&versionId="+version;
            var payload = new Dictionary<string, object>() { };

            string responseData = doGet(contextPath, apiPath, query, payload);
            //TODO:  convert responseData to a list of cycles, and search them for a matching name
            if (responseData.Contains(CycleName))
                return true;
            else
                return false;
        }


        /// <summary>
        /// zephyr says the API does not exist
        /// </summary>
        /// <param name="Project"></param>
        /// <returns></returns>
        public string CreateNewTestCycle(string Project)
        {
            PROJECT = Project;
            //create the cycle's name
            CYCLE = DateTime.Now.ToString("yyyyMMddHHmmss");
            //connect to Jira to get project and version IDs using the Jira.SDK library
            Jira.SDK.Jira jira = new Jira.SDK.Jira();
            jira.Connect(jiraBaseUrl, User, Password);
            Project webPortal = jira.GetProject(PROJECT);
            long id = webPortal.ID;
            long version = webPortal.ProjectVersions.First<ProjectVersion>().ID;

            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/cycle";
            string query = "expand=&clonedCycleId=";            

            var payload = new Dictionary<string, object>()
                {
                    { "name",  CYCLE},
                    { "build", ""},
                    { "environment", "dev"},
                    { "description", "automated tests"},
                    { "projectId", id},
                    { "versionId", version}
                };

            string responseData = doPost(contextPath, apiPath, query, payload);
            return responseData;
        }


        public void AddTestToCycle(string TestName)
        {
            //connect to Jira to get project and version IDs using the Jira.SDK library
            Jira.SDK.Jira jira = new Jira.SDK.Jira();
            jira.Connect(jiraBaseUrl, User, Password);
            Project webPortal = jira.GetProject(PROJECT);
            long id = webPortal.ID;
            long version = webPortal.ProjectVersions.First<ProjectVersion>().ID;
            string cycleId = jira.SearchIssues("project = " + PROJECT + " AND summary=" + CYCLE).First<Issue>().Key;

            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/executions/add/cycle";
            string query = cycleId;

            var payload = new Dictionary<string, object>()
            {
                { "issues",  TestName},
                { "versionId", version},
                { "projectId", id},
                { "method", "1"},  //specifies if the test is being added individually, by JQL or cloned from another cycle 
            };
            string responseData = doPost(contextPath, apiPath, query, payload);
        }


        public void UpdateTest(string TestName)
        {

        }


        private string CreateTestStep(string TestName, string StepName)
        {
            return "";
        }


        public void UpdateTestStep(string TestName, string StepName, bool Status, string Comment)
        {
            string stepId = GetTestStep(TestName, StepName);
            if (stepId.Length == 0)
            {
                stepId = CreateTestStep(TestName, StepName);
            }
            //write the results information to the step
            //connect to Jira to get project and version IDs using the Jira.SDK library
            Jira.SDK.Jira jira = new Jira.SDK.Jira();
            jira.Connect(jiraBaseUrl, User, Password);
            Project proj = jira.GetProject(PROJECT);
            long projectId = proj.ID;
            long version = proj.ProjectVersions.First<ProjectVersion>().ID;
            string issueKey = jira.SearchIssues("project = " + PROJECT + " AND summary=" + TestName).First<Issue>().Key;
            string teststepId = jira.SearchIssues("project = " + PROJECT + " AND summary=" + TestName).First<Issue>().Key;
            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/teststep/"+issueKey+"/"+teststepId;
            string query = "projectId="+projectId;

            var payload = new Dictionary<string, object>()
            {
                { "issues",  TestName},
                { "versionId", version},
                { "projectId", projectId},
                { "method", "1"},  //specifies if the test is being added individually, by JQL or cloned from another cycle 
            };
            string responseData = doPost(contextPath, apiPath, query, payload);
        }


        public string GetTestStep(string TestName, string StepName)
        {
            //connect to Jira to get project and version IDs using the Jira.SDK library
            Jira.SDK.Jira jira = new Jira.SDK.Jira();
            jira.Connect(jiraBaseUrl, User, Password);
            Project webPortal = jira.GetProject(PROJECT);
            long projectId = webPortal.ID;
            long version = webPortal.ProjectVersions.First<ProjectVersion>().ID;
            string issueKey = jira.SearchIssues("project = " + PROJECT + " AND summary=" + TestName).First<Issue>().Key;

            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/teststep/" + issueKey;
            string query = "projectId=" + projectId;
            var payload = new Dictionary<string, object>() { };

            string responseData = doGet(contextPath, apiPath, query, payload);
            return responseData;
        }



        public bool GetAllTestSteps(string TestName)
        {
            //connect to Jira to get project and version IDs using the Jira.SDK library
            Jira.SDK.Jira jira = new Jira.SDK.Jira();
            jira.Connect(jiraBaseUrl, User, Password);
            Project webPortal = jira.GetProject(PROJECT);
            long projectId = webPortal.ID;
            long version = webPortal.ProjectVersions.First<ProjectVersion>().ID;
            string issueKey = jira.SearchIssues("project = "+ PROJECT + " AND summary "+"TestName").First<Issue>().Key;
            
            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/teststep/"+issueKey;
            string query = "projectId=" + projectId;
            var payload = new Dictionary<string, object>(){};

            string responseData = doGet(contextPath, apiPath, query, payload);
            return true;
        }


        public bool GetTeststepStatuses()
        {
            string contextPath = "/connect";
            string apiPath = "/public/rest/api/1.0/teststep/statuses";
            string query = "";
            var payload = new Dictionary<string, object>(){};

            string responseData = doGet(contextPath, apiPath, query, payload);
            return true;

        }


        private string doGet(string ContextPath, string ApiPath, string Query, Dictionary<string, object> Payload)
        {
            //format the Zephyr request and use it to generate a JWT
            string zephyrBaseUrl = "https://prod-api.zephyr4jiracloud.com";
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long issuedAt = (long)Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);
            long expiresAt = issuedAt + 3600L;
            string method = "GET";

            string canonical_request = method + "&" + ApiPath + "&" + Query; 
            string signed_canonical_requestByte = getQSH(canonical_request);

            Payload.Add("sub", User);
            Payload.Add("qsh", signed_canonical_requestByte);
            Payload.Add("iss", AccessKey);
            Payload.Add("iat", issuedAt);
            Payload.Add("exp", expiresAt);

            string token = JsonWebToken.Encode(Payload, Secret, JwtHashAlgorithm.HS256);
            string decode = JsonWebToken.Decode(token, Secret);

            string request = zephyrBaseUrl + ContextPath + ApiPath;
            if (Query.Length > 0) request = request + "?" + Query;

            //put the JWT and the accesskeys in the header and request the info
            using (var httpClient = new HttpClient { BaseAddress = new Uri(request) })
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Add("USER-AGENT", "NCT Rest Client");
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "JWT " + token);
                httpClient.DefaultRequestHeaders.Add("zapiAccessKey", AccessKey);

                //  using (var response = httpClient.GetAsync(string.Format(ContextPath + ApiPath)).Result)
                using (var response = httpClient.GetAsync(string.Format(ContextPath + ApiPath)).Result)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }
        }


        private string doPost(string ContextPath, string ApiPath, string Query, Dictionary<string, object> Payload)
        {
            //format the Zephyr request and use it to generate a JWT
            string zephyrBaseUrl = "https://prod-api.zephyr4jiracloud.com";
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long issuedAt = (long)Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);
            long expiresAt = issuedAt + 3600L;
            string method = "POST";

            string canonical_request = method + "&" + ApiPath; // + "&" + query; 
            string signed_canonical_requestByte = getQSH(canonical_request);

            Payload.Add("sub", User);
            Payload.Add("qsh", signed_canonical_requestByte);
            Payload.Add("iss", AccessKey);
            Payload.Add("iat", issuedAt);
            Payload.Add("exp", expiresAt);

            string token = JsonWebToken.Encode(Payload, Secret, JwtHashAlgorithm.HS256);
            string decode = JsonWebToken.Decode(token, Secret);

            //put the JWT and the accesskeys in the header and request the info
            using (var httpClient = new HttpClient { BaseAddress = new Uri(zephyrBaseUrl) })
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "JWT " + token);
                httpClient.DefaultRequestHeaders.Add("zapiAccessKey", AccessKey);
                var content = new StringContent(token, System.Text.Encoding.Default, "application/json");
                using (var response = httpClient.PostAsync(string.Format(ContextPath + ApiPath), content).Result)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }
        }


        private string doPut(string ContextPath, string ApiPath, string Query, Dictionary<string, object> Payload)
        {
            //format the Zephyr request and use it to generate a JWT
            string zephyrBaseUrl = "https://prod-api.zephyr4jiracloud.com";
            var unixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            long issuedAt = (long)Math.Round((DateTime.UtcNow - unixEpoch).TotalSeconds);
            long expiresAt = issuedAt + 3600L;
            string method = "PUT";

            string canonical_request = method + "&" + ApiPath; // + "&" + query; 
            string signed_canonical_requestByte = getQSH(canonical_request);

            Payload.Add("sub", User);
            Payload.Add("qsh", signed_canonical_requestByte);
            Payload.Add("iss", AccessKey);
            Payload.Add("iat", issuedAt);
            Payload.Add("exp", expiresAt);

            string token = JsonWebToken.Encode(Payload, Secret, JwtHashAlgorithm.HS256);

            //put the JWT and the accesskeys in the header and request the info
            using (var httpClient = new HttpClient { BaseAddress = new Uri(zephyrBaseUrl) })
            {
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "JWT " + token);
                httpClient.DefaultRequestHeaders.Add("zapiAccessKey", AccessKey);

                var content = new StringContent(token, System.Text.Encoding.Default, "application/json");
                using (var response = httpClient.PutAsync(string.Format(ContextPath + ApiPath), content).Result)
                {
                    return response.Content.ReadAsStringAsync().Result;
                }
            }
        }

        public static String getQSH(String value)
        {
            StringBuilder Sb = new StringBuilder();

            using (System.Security.Cryptography.SHA256 hash = System.Security.Cryptography.SHA256Managed.Create())
            {
                Encoding enc = Encoding.UTF8;
                Byte[] result = hash.ComputeHash(enc.GetBytes(value));

                foreach (Byte b in result)
                    Sb.Append(b.ToString("x2"));
            }

            return Sb.ToString();
        }


    }
}
